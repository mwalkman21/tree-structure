package com.uwtree.structure.model;

import lombok.Data;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

@NodeEntity
@Data
public class Document {

  @Id
  @GeneratedValue
  private Long id;
  private String title;
  private String text;
  @Relationship(type = "HAS_A_CHILD")
  private Set<Document> relatedDocuments;
}
