package com.uwtree.structure.config;

import com.google.common.collect.Sets;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;
import java.time.OffsetDateTime;

/**
 * Swagger configuration.
 *
 * @author ostap.pidluzhnyi
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Bean
  public Docket api(ApiInfo apiInfo) {
    return new Docket(DocumentationType.SWAGGER_2).directModelSubstitute(LocalDate.class, java.sql.Date.class)
        .directModelSubstitute(OffsetDateTime.class, java.util.Date.class)
        .consumes(Sets.newHashSet(MediaType.APPLICATION_JSON_VALUE))
        .produces(Sets.newHashSet(MediaType.APPLICATION_JSON_VALUE))
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.uwtree.structure.controller"))
        .paths(PathSelectors.any())
        .build()
        .apiInfo(apiInfo)
        .useDefaultResponseMessages(false);
  }

  @Bean
  public ApiInfo apiInfo() {
    return new ApiInfoBuilder().title("Neo4j Tree Api")
        .version("0.0.1-SNAPSHOT")
        .build();
  }

  @Bean
  public Contact swaggerContact() {
    return new Contact("Mykhailo Stefantsiv", "gitlab-url", "mwalkman21@gmail.com");
  }
}
