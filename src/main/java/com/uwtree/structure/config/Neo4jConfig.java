package com.uwtree.structure.config;

import org.neo4j.ogm.session.SessionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

@Configuration
@EnableNeo4jRepositories(basePackages = "com.uwtree.structure.repository")
public class Neo4jConfig {

  @Value("${spring.neo4j.uri}")
  private String url;
  @Value("${spring.data.neo4j.username}")
  private String username;
  @Value("${spring.data.neo4j.password}")
  private String password;

  @Bean
  public SessionFactory sessionFactory() {
    org.neo4j.ogm.config.Configuration configuration = new org.neo4j.ogm.config.Configuration.Builder()
        .uri(url)
        .credentials(username, password)
        .build();
    return new SessionFactory(configuration, "com.uwtree.structure");
  }

}
