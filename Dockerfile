FROM openjdk:11-jre-slim

COPY ./target/structure-*.jar /service.jar

COPY ./docker-entrypoint.sh /
ENTRYPOINT ["sh", "/docker-entrypoint.sh"]