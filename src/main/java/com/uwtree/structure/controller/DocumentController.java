package com.uwtree.structure.controller;

import com.uwtree.structure.model.Document;
import com.uwtree.structure.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/document")
public class DocumentController {

  private final DocumentService documentService;

  @Autowired
  public DocumentController(DocumentService documentService) {
    this.documentService = documentService;
  }

  @Async
  @GetMapping("/{id}")
  @ResponseStatus(value = HttpStatus.OK)
  public CompletableFuture<Document> findOne(@PathVariable String id) {
    return CompletableFuture.supplyAsync(() -> documentService.findDocument(Long.valueOf(id)));
  }

  @PostMapping("/{id}/add")
  @ResponseStatus(value = HttpStatus.OK)
  public void addDocument(@PathVariable String id, @RequestBody Document document) {
    documentService.addNode(Long.valueOf(id), document);
  }

  @PostMapping
  @ResponseStatus(value = HttpStatus.OK)
  public void addDocument(@RequestBody Document document) {
    documentService.addDocument(document);
  }

  @DeleteMapping("/clear")
  @ResponseStatus(value = HttpStatus.OK)
  public void deleteAll() {
    documentService.deleteAll();
  }
}
