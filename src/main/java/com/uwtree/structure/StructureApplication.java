package com.uwtree.structure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
@ComponentScan("com.uwtree.structure")
public class StructureApplication {

  public static void main(String[] args) {
    SpringApplication.run(StructureApplication.class, args);
  }

}
