#!/usr/bin/env bash

: ${SLEEP_LENGTH:=2}
: ${TIMEOUT_LENGTH:=300}

wait_for() {
  START=$(date +%s)
  echo "Waiting for $1:$2..."
  while ! nc -z $1 $2;
    do
    if [ $(($(date +%s) - $START)) -gt ${TIMEOUT_LENGTH} ]; then
        echo "Service $1:$2 did not start within $TIMEOUT_LENGTH seconds. Aborting..."
        exit 1
    fi
    sleep ${SLEEP_LENGTH}
  done
}

for service in "$@"
do
  host=${service%:*}
  port=${service#*:}
  wait_for ${host} ${port}
  echo "$host:$port is ready!"
done

java ${JAVA_OPTS} -jar /service.jar --server.port=80