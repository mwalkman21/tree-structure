package com.uwtree.structure.service;

import com.uwtree.structure.model.Document;
import com.uwtree.structure.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;

@Component
public class DocumentService {

  private final DocumentRepository documentRepository;

  @Autowired
  public DocumentService(DocumentRepository documentRepository) {
    this.documentRepository = documentRepository;
  }

  @Transactional
  public void addNode(Long parentId, Document document) {
    //depth 0 here, to avoid retrieving child nodes
    Optional<Document> parent = documentRepository.findById(parentId, 0);
    if (parent.isPresent()) {
      Document newDocument = documentRepository.save(document);
      Document parentDocument = parent.get();
      if (parentDocument.getRelatedDocuments() == null) {
        parentDocument.setRelatedDocuments(new HashSet<>());
      }
      parentDocument.getRelatedDocuments().add(newDocument);
      documentRepository.save(parentDocument);
    } else {
      throw new IllegalStateException("Parent document not found by id: " + parentId);
    }
  }

  @Transactional
  public void addDocument(Document document) {
    documentRepository.save(document);
  }

  @Transactional
  public Document findDocument(Long id) {
    Optional<Document> document = documentRepository.findById(id, -1);//depth set to -1 to retrieve whole tree
    if (document.isPresent()) {
      return document.get();
    } else {
      throw new IllegalStateException("Document was not found by id: " + id);
    }
  }

  public void deleteAll() {
    documentRepository.deleteAll();
  }

}
