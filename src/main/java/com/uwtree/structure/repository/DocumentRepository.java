package com.uwtree.structure.repository;

import com.uwtree.structure.model.Document;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentRepository extends Neo4jRepository<Document, Long> {

}
